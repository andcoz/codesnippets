# C/C++ Code Snippets

Some simple examples, useful for teaching basics of Unix API

* [NFSLockTest](NFSLockTest): stess test for lock file over nfs
* [SendMail](SendMail): forks a child, connects pipes to father and execs `sendmail` controlled by father
