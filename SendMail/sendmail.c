#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>

int main(){
  int pipe_endpoints[2] = { 0, 0 };

  char * const mail_argv[] = { "mail", "-s", "test_mail", "andcoz", NULL };
  char * const mail_body[] = { "test mesg\n", "i am a test\n", "test\n", NULL };
  
  /* prepare a pipe to feed "mail" process */
  if (pipe(pipe_endpoints) == 0) {

    /* spawn a child process */
    if (fork() == 0) {
      /* child process */

      /* connect the pipe to standard input */
      close(pipe_endpoints[1]);
      dup2(pipe_endpoints[0], 0);
      
      /* exec mail program */
      execvp(mail_argv[0], mail_argv);
    } else {
      /* father process */
      close(pipe_endpoints[0]);

      /* write mail body to pipe */
      int i = 0;

      while (mail_body[i] != NULL) {
	write(pipe_endpoints[1], mail_body[i], strlen(mail_body[i]));
	i++;
      }

      /* write a single dot on the last line */
      /* write(pipe_endpoints[1], ".\n", 2); */

      close(pipe_endpoints[1]);

      /* wait for child ends */
      wait(NULL);
    }
  }
  
  return 0;
}
