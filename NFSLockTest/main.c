#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

# define EVER ;;

int main(int argc, char **argv) {
  if (argc < 4) return -1;

  char *filename = argv[1];
  char *myself = argv[2];
  unsigned long delta = strtoul(argv[3], NULL, 10);
  delta = (delta != 0) ? delta : 5;

  int status = 0;

  for(EVER) {
    printf("opening\n");
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT);

    if (fd == -1) {
      perror(argv[0]);
    } else {
      if (status) {
	struct flock fl;
	
	fl.l_type = F_WRLCK;
	fl.l_start = 0;
	fl.l_whence = SEEK_SET;
	fl.l_len = 0;
	fl.l_pid = 0;
	
	printf("locking\n");
	if (fcntl(fd, F_SETLK, &fl) != 0) {
	  perror(argv[0]);
	} else {
	  char buffer[1024];

	  time_t now = time(NULL);
	  ctime_r(&now, buffer); 
	  buffer[strlen(buffer) - 1] = '\0';

	  strcat(buffer, ": ");
	  strcat(buffer, myself);
	  strcat(buffer, "\n");
	  int buff_size = strlen(buffer);

	  printf("writing\n");
	  if (write(fd, buffer, buff_size) < buff_size) {
	    perror(argv[0]);
	  }
	}
      } else {
	/* no op */
      }
      status = ! status;
    }

    printf("sleeping\n");
    sleep(delta);
    printf("closing\n");
    close(fd);
  }

  return -2;
}
